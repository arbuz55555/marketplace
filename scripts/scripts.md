curl -XPOST http://localhost:8080/order -d "{\"userId\":\"123\",\"price\":\"15.1\",\"orderType\":\"BUY\",\"quantity\":\"32.1\",\"coinType\":\"LITECOIN\"}" -H "Content-Type:application/json"

curl -XDELETE http://localhost:8080/order -d "{\"userId\":\"123\",\"price\":\"15.1\",\"orderType\":\"BUY\",\"quantity\":\"32.1\",\"coinType\":\"LITECOIN\"}" -H "Content-Type:application/json"

curl -XGET http://localhost:8080/orderSummary?orderType=BUY

curl -XGET http://localhost:8080/orderSummary?orderType=SELL