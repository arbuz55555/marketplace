Feature: User can submit and delete marketplace order

  Scenario: User can submit the order
    When The user submits the order
    Then The order is persisted in the marketplace
    And The order is visible on the summary board

  Scenario: User can submit multiple orders
    When The user submits multiple orders
    Then The orders are visible on the summary board

  Scenario: User can delete the order
    Given the user with the order
    When The user deletes the order
    Then The order is deleted
    And The order is not visible on the summary board

  Scenario: User can't delete the order which does not exist
    Given The user without any order
    When The user deletes the order
    Then The user is not able to delete not existing order
