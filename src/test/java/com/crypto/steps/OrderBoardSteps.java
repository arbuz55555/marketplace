package com.crypto.steps;

import com.crypto.AbstractStep;
import com.crypto.controller.dto.OrderBoardResponse;
import com.crypto.controller.dto.OrderBoardResponseItem;
import com.crypto.controller.dto.OrderItemRequest;
import com.crypto.domain.OrderItem;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

import static com.crypto.testdata.OrderItemRequestDataFactory.sellOrderItemRequest;
import static org.assertj.core.api.Assertions.assertThat;

public class OrderBoardSteps extends AbstractStep {

    private static final String ORDER_URL = "/order";

    private ResponseEntity<String> response;

    @Before
    public void before() {
        sellMap.clear();
        buyMap.clear();
        orderMap.clear();
    }

    @Autowired
    @Qualifier("sellMap")
    private ConcurrentSkipListMap<BigDecimal, BigDecimal> sellMap;

    @Autowired
    @Qualifier("buyMap")
    private ConcurrentSkipListMap<BigDecimal, BigDecimal> buyMap;

    @Autowired
    @Qualifier("orderMap")
    private ConcurrentHashMap<OrderItem, Integer> orderMap;

    @Given("The user with the order")
    public void the_user_the_order(){
        orderMap.put(OrderItem.from(sellOrderItemRequest()), 1);
    }

    @Given("The user without any order")
    public void the_user_without_any_order(){
    }

    @When("The user submits the order")
    public void the_user_submits_the_order(){
        response = restTemplate.postForEntity("/order", sellOrderItemRequest(), String.class);
    }

    @When("The user deletes the order")
    public void the_user_deletes_the_order(){
        response = restTemplate.exchange(ORDER_URL, HttpMethod.DELETE, new HttpEntity<>(sellOrderItemRequest()), String.class);
    }

    @When("The user submits multiple orders")
    public void the_user_submits_multiple_order(){
        OrderItemRequest orderItemRequest = sellOrderItemRequest();
        restTemplate.postForEntity(ORDER_URL, orderItemRequest, String.class);
        restTemplate.postForEntity(ORDER_URL, orderItemRequest, String.class);
    }

    @Then("The order is deleted")
    public void the_order_is_deleted() {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(orderMap.get(sellOrderItemRequest())).isNull();
    }

    @Then("The order is persisted in the marketplace")
    public void the_order_is_persisted_in_the_marketplace() {
        OrderItemRequest orderItemRequest = sellOrderItemRequest();
        OrderItem orderItem = convert(response.getBody(), OrderItem.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(orderItem).isEqualTo(OrderItem.from(orderItemRequest));
        assertThat(sellMap.get(orderItemRequest.getPrice())).isNotNull();
    }

    @Then("The user is not able to delete not existing order")
    public void the_user_is_not_able_to_delete_not_existing_order() {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @And("The order is visible on the summary board")
    public void the_order_is_visible_on_the_summary_board() {
        OrderItemRequest orderItemRequest = sellOrderItemRequest();
        OrderBoardResponseItem expected = new OrderBoardResponseItem(orderItemRequest.getPrice(), orderItemRequest.getQuantity());
        OrderBoardResponse orderBoardResponse = restTemplate.getForObject("/orderSummary?orderType=SELL", OrderBoardResponse.class);
        assertThat(orderBoardResponse.getSummary().get(0)).isEqualTo(expected);
    }

    @And("The orders are visible on the summary board")
    public void the_orders_are_visible_on_the_summary_board() {
        OrderItemRequest orderItemRequest = sellOrderItemRequest();
        OrderBoardResponseItem expected = new OrderBoardResponseItem(orderItemRequest.getPrice(), orderItemRequest.getQuantity().add(orderItemRequest.getQuantity()));
        OrderBoardResponse orderBoardResponse = restTemplate.getForObject("/orderSummary?orderType=SELL", OrderBoardResponse.class);
        assertThat(orderBoardResponse.getSummary().get(0)).isEqualTo(expected);
    }

    @And("The order is not visible on the summary board")
    public void the_order_is_not_visible_on_the_summary_board() {
        OrderBoardResponse orderBoardResponse = restTemplate.getForObject("/orderSummary?orderType=SELL", OrderBoardResponse.class);
        assertThat(orderBoardResponse.getSummary().size()).isEqualTo(0);
    }

}
