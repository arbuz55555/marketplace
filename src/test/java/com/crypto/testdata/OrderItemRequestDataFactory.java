package com.crypto.testdata;

import com.crypto.controller.dto.OrderItemRequest;
import com.crypto.domain.CoinType;
import com.crypto.domain.OrderType;

import java.math.BigDecimal;

public class OrderItemRequestDataFactory {

    public static OrderItemRequest sellOrderItemRequest() {
        return OrderItemRequest.builder()
                .coinType(CoinType.LITECOIN)
                .price(BigDecimal.valueOf(12.3))
                .quantity(BigDecimal.valueOf(10))
                .userId(12)
                .orderType(OrderType.SELL)
                .build();
    }

}
