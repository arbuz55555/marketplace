package com.crypto.testdata;

import com.crypto.controller.dto.OrderBoardResponseItem;
import com.crypto.domain.OrderBoardItem;

import java.math.BigDecimal;

public class OrderBoardItemDataFactory {

    public static OrderBoardItem orderBoardItem() {
        return OrderBoardItem.builder()
                .price(BigDecimal.valueOf(12))
                .totalQuantity(BigDecimal.valueOf(2.5))
                .build();
    }

}
