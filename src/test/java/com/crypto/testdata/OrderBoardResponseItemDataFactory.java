package com.crypto.testdata;

import com.crypto.controller.dto.OrderBoardResponseItem;

import java.math.BigDecimal;

public class OrderBoardResponseItemDataFactory {

    public static OrderBoardResponseItem orderBoardResponseItem() {
        return new OrderBoardResponseItem(BigDecimal.valueOf(12), BigDecimal.valueOf(2.5));
    }

}
