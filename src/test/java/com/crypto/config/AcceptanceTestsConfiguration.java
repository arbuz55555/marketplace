package com.crypto.config;

import com.crypto.domain.OrderItem;
import com.crypto.repository.InMemoryOrderBoardRepository;
import com.crypto.repository.InMemoryOrderRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Profile("acceptance")
@Configuration
public class AcceptanceTestsConfiguration {

    @Bean("sellMap")
    public ConcurrentSkipListMap<BigDecimal, BigDecimal> sellMap() {
        return new ConcurrentSkipListMap<BigDecimal, BigDecimal>();
    }

    @Bean("buyMap")
    public ConcurrentSkipListMap<BigDecimal, BigDecimal> buyMap() {
        return new ConcurrentSkipListMap<BigDecimal, BigDecimal>();
    }

    @Bean("orderMap")
    public ConcurrentMap<OrderItem, Integer> orderMap() {
        return new ConcurrentHashMap<OrderItem, Integer>();
    }

    @Bean
    public InMemoryOrderBoardRepository inMemoryOrderBoardRepository() {
        return new InMemoryOrderBoardRepository(sellMap(), buyMap());
    }

    @Bean
    public InMemoryOrderRepository inMemoryOrderRepository() {
        return new InMemoryOrderRepository(orderMap());
    }
}