 package com.crypto.repository;

import com.crypto.domain.OrderBoardItem;
import com.crypto.domain.OrderItem;
import com.crypto.domain.OrderType;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;

import static com.crypto.testdata.OrderItemDataFactory.orderItem;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

 @RunWith(MockitoJUnitRunner.class)
public class InMemoryOrderBoardRepositoryTest {

    private final ConcurrentSkipListMap<BigDecimal, BigDecimal> sellMap = new ConcurrentSkipListMap();
    private final ConcurrentSkipListMap<BigDecimal, BigDecimal> buyMap = new ConcurrentSkipListMap();

    private InMemoryOrderBoardRepository inMemoryOrderBoardRepository = new InMemoryOrderBoardRepository(sellMap, buyMap);

    @After
    public void after() {
        sellMap.clear();
        buyMap.clear();
    }

    @Test
    public void shouldInsert() {

        // Given

        OrderItem orderItem = orderItem();

        // When

        inMemoryOrderBoardRepository.insert(orderItem);
        inMemoryOrderBoardRepository.insert(orderItem);

        // Then

        assertThat(sellMap.get(orderItem.getPrice())).isEqualTo(orderItem.getQuantity().add(orderItem.getQuantity()));
    }

     @Test
     public void shouldDelete() {

         // Given

         OrderItem orderItem = orderItem();

         // When

         inMemoryOrderBoardRepository.insert(orderItem);
         inMemoryOrderBoardRepository.delete(orderItem);

         // Then

         assertThat(sellMap.get(orderItem.getPrice())).isNull();
     }

     @Test
     public void shouldProvideSummary() {

         // Given

         OrderItem orderItem = orderItem();
         OrderBoardItem expected = OrderBoardItem.builder()
            .price(orderItem.getPrice())
            .totalQuantity(orderItem.getQuantity().add(orderItem.getQuantity()))
            .build();

         // When

         inMemoryOrderBoardRepository.insert(orderItem);
         inMemoryOrderBoardRepository.insert(orderItem);
         List<OrderBoardItem> result = inMemoryOrderBoardRepository.summary(OrderType.SELL, 10);

         // Then

         assertThat(result).isEqualTo(Arrays.asList(expected));
     }

}
