package com.crypto.repository;

import com.crypto.domain.OrderItem;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.crypto.testdata.OrderItemDataFactory.orderItem;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class InMemoryOrderRepositoryTest {

    private ConcurrentMap<OrderItem, Integer> concurrentMap = new ConcurrentHashMap<>();

    private InMemoryOrderRepository orderRepository = new InMemoryOrderRepository(concurrentMap);

    @After
    public void after() {
        concurrentMap.clear();
    }

    @Test
    public void shouldInsert() {

        // Given

        OrderItem expected = orderItem();

        // When

        OrderItem result = orderRepository.insert(expected);

        // Then

        assertThat(result).isEqualTo(expected);
        assertThat(concurrentMap.get(expected)).isEqualTo(1);

    }

    @Test
    public void shouldDelete() {

        // Given

        OrderItem expected = orderItem();
        concurrentMap.put(expected, 1);

        // When

        orderRepository.delete(expected);

        // Then

        assertThat(concurrentMap.get(expected)).isNull();

    }

    @Test
    public void shouldCheckIfTheEntryExists() {

        // Given

        OrderItem expected = orderItem();
        concurrentMap.put(expected, 1);

        // When

        boolean result = orderRepository.exists(expected);

        // Then

        assertThat(result).isTrue();

    }

}
