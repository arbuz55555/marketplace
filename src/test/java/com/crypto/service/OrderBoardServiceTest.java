package com.crypto.service;

import com.crypto.domain.OrderBoardItem;
import com.crypto.domain.OrderType;
import com.crypto.repository.OrderBoardRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.crypto.testdata.OrderBoardItemDataFactory.orderBoardItem;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class OrderBoardServiceTest {

    @Mock
    private OrderBoardRepository orderBoardRepository;

    @InjectMocks
    private OrderBoardService orderBoardService;

    @Test
    public void shouldRetrieveSummary() {

        // Given

        List<OrderBoardItem> expected = Arrays.asList(orderBoardItem());
        given(orderBoardRepository.summary(OrderType.SELL, 10)).willReturn(expected);

        // When

        List<OrderBoardItem> result = orderBoardService.summary(OrderType.SELL, 10);

        // Then

        assertThat(result).isEqualTo(expected);

    }

}
