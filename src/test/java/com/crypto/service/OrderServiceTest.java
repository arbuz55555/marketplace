package com.crypto.service;

import com.crypto.domain.OrderItem;
import com.crypto.repository.OrderBoardRepository;
import com.crypto.repository.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.crypto.testdata.OrderItemDataFactory.orderItem;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderBoardRepository orderBoardRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void shouldSave() {

        // Given

        OrderItem expected = orderItem();

        // When

        OrderItem result = orderService.save(expected);

        // Then

        verify(orderRepository).insert(expected);
        verify(orderBoardRepository).insert(expected);
        assertThat(result).isEqualTo(expected);

    }

    @Test
    public void shouldDelete() {

        // Given

        OrderItem expected = orderItem();

        // When

        orderService.delete(expected);

        // Then

        verify(orderRepository).delete(expected);
        verify(orderBoardRepository).delete(expected);
    }

}
