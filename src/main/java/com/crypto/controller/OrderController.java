package com.crypto.controller;

import com.crypto.service.OrderService;
import com.crypto.controller.dto.OrderItemRequest;
import com.crypto.controller.exception.OrderItemNotFoundException;
import com.crypto.domain.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public OrderItem create(@Valid @RequestBody OrderItemRequest orderItemRequest) {
        return orderService.save(OrderItem.from(orderItemRequest));
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @RequestBody OrderItemRequest orderItemRequest) {
        OrderItem orderItem = OrderItem.from(orderItemRequest);
        if (!orderService.exists(orderItem)) {
            throw new OrderItemNotFoundException();
        }
        orderService.delete(orderItem);
    }
}