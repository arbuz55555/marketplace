package com.crypto.controller;

import com.crypto.service.OrderBoardService;
import com.crypto.controller.dto.OrderBoardResponse;
import com.crypto.controller.dto.OrderBoardResponseItem;
import com.crypto.controller.exception.OrderTypeNotFoundException;
import com.crypto.domain.OrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.EnumSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orderSummary")
public class OrderBoardController {

    private static final int SUMMARY_RECORDS_NUMBER = 10;
    private final OrderBoardService orderBoardService;

    @Autowired
    public OrderBoardController(OrderBoardService orderBoardService) {
        this.orderBoardService = orderBoardService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public OrderBoardResponse summary(@RequestParam String orderType) {
        if (!EnumSet.allOf(OrderType.class).stream()
                .map(Enum::name)
                .collect(Collectors.toList())
                .contains(orderType.toUpperCase())) {
            throw new OrderTypeNotFoundException();
        }
        return OrderBoardResponse.builder()
                .summary(
                        orderBoardService.summary(OrderType.valueOf(orderType.toUpperCase()), SUMMARY_RECORDS_NUMBER)
                                .stream().map(OrderBoardResponseItem::from)
                                .collect(Collectors.toList())
                )
                .build();
    }

}
