package com.crypto.controller.dto;

import com.crypto.domain.OrderBoardItem;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
public class OrderBoardResponseItem {

    BigDecimal price;
    BigDecimal totalQuantity;

    public static OrderBoardResponseItem from(OrderBoardItem orderBoardItem) {
        return new OrderBoardResponseItem(orderBoardItem.getPrice(), orderBoardItem.getTotalQuantity());
    }
}
