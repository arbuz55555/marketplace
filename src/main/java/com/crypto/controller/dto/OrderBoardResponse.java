package com.crypto.controller.dto;

import com.crypto.domain.OrderBoardItem;
import com.crypto.domain.OrderItem;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class OrderBoardResponse {

    List<OrderBoardResponseItem> summary;

}
