package com.crypto.controller.dto;

import com.crypto.domain.CoinType;
import com.crypto.domain.OrderType;
import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
@Builder
public class OrderItemRequest {

    long userId;

    @NotNull
    OrderType orderType;

    @NotNull
    CoinType coinType;

    @DecimalMin(value = "0.0001", inclusive = false)
    BigDecimal quantity;

    @DecimalMin(value = "0.0001", inclusive = false)
    BigDecimal price;

}
