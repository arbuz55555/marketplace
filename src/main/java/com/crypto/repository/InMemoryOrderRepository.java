package com.crypto.repository;

import com.crypto.domain.OrderItem;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Repository class for order related operations.
 */
@Repository
@Profile("!acceptance")
public class InMemoryOrderRepository implements OrderRepository {

    private final ConcurrentMap<OrderItem, Integer> map;

    public InMemoryOrderRepository() {
        map = new ConcurrentHashMap<>();
    }

    public InMemoryOrderRepository(ConcurrentMap<OrderItem, Integer> map) {
        this.map = map;
    }

    /**
     * Insert the new order.
     */
    public synchronized OrderItem insert(OrderItem orderItem) {
        map.put(orderItem, map.getOrDefault(orderItem, 0) + 1);
        return orderItem;
    }

    /**
     * Delete the existing order.
     */
    public synchronized void delete(OrderItem orderItem) {
        map.put(orderItem, map.get(orderItem) - 1);
        if (map.get(orderItem) == 0) {
            map.remove(orderItem);
        }
    }

    /**
     * Check if the order does exist.
     */
    public boolean exists(OrderItem orderItem) {
        return map.containsKey(orderItem);
    }
}