package com.crypto.repository;

import com.crypto.domain.OrderItem;

public interface OrderRepository {

    OrderItem insert(OrderItem orderItem);

    void delete(OrderItem orderItem);

    boolean exists(OrderItem orderItem);

}
