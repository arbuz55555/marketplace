package com.crypto.repository;

import com.crypto.domain.OrderBoardItem;
import com.crypto.domain.OrderItem;
import com.crypto.domain.OrderType;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

/**
 * Repository class for Order board related operations.
 */
@Repository
@Profile("!acceptance")
public class InMemoryOrderBoardRepository implements OrderBoardRepository {

    private final ConcurrentSkipListMap<BigDecimal, BigDecimal> sellMap;
    private final ConcurrentSkipListMap<BigDecimal, BigDecimal> buyMap;

    public InMemoryOrderBoardRepository() {
        this.buyMap = new ConcurrentSkipListMap(Comparator.reverseOrder());
        this.sellMap = new ConcurrentSkipListMap();
    }

    public InMemoryOrderBoardRepository(ConcurrentSkipListMap<BigDecimal, BigDecimal> sellMap, ConcurrentSkipListMap<BigDecimal, BigDecimal> buyMap) {
        this.sellMap = sellMap;
        this.buyMap = buyMap;
    }

    /**
     * Create or update Order board record.
     *
     */
    public OrderBoardItem insert(OrderItem orderItem) {
        ConcurrentSkipListMap<BigDecimal, BigDecimal> map = getMap(orderItem.getOrderType());
        map.put(orderItem.getPrice(), map.getOrDefault(orderItem.getPrice(), BigDecimal.ZERO).add(orderItem.getQuantity()));
        return OrderBoardItem.builder()
                .price(orderItem.getPrice())
                .totalQuantity(map.get(orderItem.getPrice()))
                .build();
    }

    /**
     * Delete or update order board record.
     */
    public void delete(OrderItem orderItem) {
        updateOrRemove(getMap(orderItem.getOrderType()), orderItem.getPrice(), orderItem.getQuantity());
    }

    /**
     * Order board summary.
     */
    public List<OrderBoardItem> summary(OrderType orderType, int limit) {
        return getMap(orderType).entrySet().stream()
                .limit(limit)
                .map(x -> OrderBoardItem.builder()
                        .price(x.getKey())
                        .totalQuantity(x.getValue())
                        .build())
                .collect(Collectors.toList());
    }

    private ConcurrentSkipListMap<BigDecimal, BigDecimal> getMap(OrderType orderType) {
        switch (orderType) {
            case SELL:
                return sellMap;
            case BUY:
                return buyMap;
            default:
                throw new RuntimeException("Not supported order type " + orderType);
        }
    }

    private synchronized void updateOrRemove(ConcurrentSkipListMap<BigDecimal, BigDecimal> map, BigDecimal price, BigDecimal quantity) {
        BigDecimal totalQuantity = map.get(price).subtract(quantity);
        map.put(price, totalQuantity);
        if (totalQuantity.equals(BigDecimal.ZERO)) {
            map.remove(price);
        }
    }
}