package com.crypto.repository;

import com.crypto.domain.OrderBoardItem;
import com.crypto.domain.OrderItem;
import com.crypto.domain.OrderType;

import java.util.List;

public interface OrderBoardRepository {

    OrderBoardItem insert(OrderItem orderItem);

    void delete(OrderItem orderItem);

    List<OrderBoardItem> summary(OrderType orderType, int limit);

}
