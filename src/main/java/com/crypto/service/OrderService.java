package com.crypto.service;

import com.crypto.domain.OrderItem;
import com.crypto.repository.OrderBoardRepository;
import com.crypto.repository.InMemoryOrderRepository;
import com.crypto.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for order operations.
 */
@Service
public class OrderService {

    private final OrderBoardRepository orderBoardRepository;

    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderBoardRepository orderBoardRepository, OrderRepository orderRepository) {
        this.orderBoardRepository = orderBoardRepository;
        this.orderRepository = orderRepository;
    }

    /**
     * Save the order and update order board.
     */
    public synchronized OrderItem save(OrderItem orderItem) {
        orderRepository.insert(orderItem);
        orderBoardRepository.insert(orderItem);
        return orderItem;
    }

    /**
     * Delete the order and update order board.
     */
    public synchronized void delete(OrderItem orderItem) {
        orderRepository.delete(orderItem);
        orderBoardRepository.delete(orderItem);
    }

    /**
     * Check if the order does exist.
     */
    public boolean exists(OrderItem orderItem) {
        return orderRepository.exists(orderItem);
    }

}


