package com.crypto.service;

import com.crypto.domain.OrderBoardItem;
import com.crypto.domain.OrderType;
import com.crypto.repository.OrderBoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for order board operations.
 */
@Service
public class OrderBoardService {

    private final OrderBoardRepository orderBoardRepository;

    @Autowired
    public OrderBoardService(OrderBoardRepository orderBoardRepository) {
        this.orderBoardRepository = orderBoardRepository;
    }

    /**
     * Return the order board summary.
     */
    public List<OrderBoardItem> summary(OrderType orderType, int limit) {
        return orderBoardRepository.summary(orderType, limit);
    }
}