package com.crypto.domain;

import com.crypto.controller.dto.OrderItemRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class OrderItem {

    long userId;

    OrderType orderType;

    CoinType coinType;

    BigDecimal quantity;

    BigDecimal price;

    public static OrderItem from(OrderItemRequest orderItemRequest) {
        return OrderItem.builder()
                .coinType(orderItemRequest.getCoinType())
                .price(orderItemRequest.getPrice())
                .quantity(orderItemRequest.getQuantity())
                .userId(orderItemRequest.getUserId())
                .orderType(orderItemRequest.getOrderType())
                .build();
    }

}
