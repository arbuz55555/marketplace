package com.crypto.domain;

public enum CoinType {
    LITECOIN, ETHEREUM, RIPPLE, TETHER, BITCOIN_CASH, LIBRA, MONERO, EOS, BITCOIN_SV, BINANCE_COIN
}
