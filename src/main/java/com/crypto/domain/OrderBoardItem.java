package com.crypto.domain;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class OrderBoardItem {

    BigDecimal price;
    BigDecimal totalQuantity;

}