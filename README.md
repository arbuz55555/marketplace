# Credit Suisse assignment for Senior Java Developer role

### The solution

Simple REST API services for submitting/deleting the orders 
and providing the live board orders summary.

NOTE. 
The author is aware about the limitations of the solution in terms of the scalability.
One of the options to build more scalable in memory solution, 
could be use https://infinispan.org/ instead of Concurrent Maps. 

### Technologies

* Spring Boot Test - Provides the spring boot ecosystem required for the tests
* AssertJ - Assertions
* Cucumber (with Spring Boot) - BDD based Integration Test and Component Tests
* Swagger - api documentation

### Running all the tests and building the project
```cmd
mvn clean install
```
### Executable jar location

target/marketplace-0.0.1-SNAPSHOT.jar

### How to test the API on your localhost

curl -XPOST http://localhost:8080/order -d "{\"userId\":\"123\",\"price\":\"15.1\",\"orderType\":\"BUY\",\"quantity\":\"32.1\",\"coinType\":\"LITECOIN\"}" -H "Content-Type:application/json"

curl -XDELETE http://localhost:8080/order -d "{\"userId\":\"123\",\"price\":\"15.1\",\"orderType\":\"BUY\",\"quantity\":\"32.1\",\"coinType\":\"LITECOIN\"}" -H "Content-Type:application/json"

curl -XGET http://localhost:8080/orderSummary?orderType=BUY

curl -XGET http://localhost:8080/orderSummary?orderType=SELL

### Swagger docs

https://marketplace-cs.ew.r.appspot.com/v2/api-docs

### Live demo (GCP appengine)

https://marketplace-cs.ew.r.appspot.com/orderSummary?orderType=buy

https://marketplace-cs.ew.r.appspot.com/orderSummary?orderType=sell

curl -XPOST https://marketplace-cs.ew.r.appspot.com/order -d "{\"userId\":\"123\",\"price\":\"15.1\",\"orderType\":\"BUY\",\"quantity\":\"32.1\",\"coinType\":\"LITECOIN\"}" -H "Content-Type:application/json"

curl -XDELETE https://marketplace-cs.ew.r.appspot.com/order -d "{\"userId\":\"123\",\"price\":\"15.1\",\"orderType\":\"BUY\",\"quantity\":\"32.1\",\"coinType\":\"LITECOIN\"}" -H "Content-Type:application/json"

### Troubleshooting

Exception in thread "main" java.lang.ClassCastException: class jdk.internal.loader.ClassLoaders$AppClassLoader cannot be cast to class java.net.URLClassLoader (jdk.internal.loader.ClassLoaders$AppClassLoader and java.net.URLClassLoader are in module java.base of loader 'bootstrap')
	at org.springframework.boot.devtools.restart.DefaultRestartInitializer.getUrls(DefaultRestartInitializer.java:93)
	at org.springframework.boot.devtools.restart.DefaultRestartInitializer.getInitialUrls(DefaultRestartInitializer.java:56)
	at org.springframework.boot.devtools.restart.Restarter.<init>(Restarter.java:140)
	at org.springframework.boot.devtools.restart.Restarter.initialize(Restarter.java:583)
	at org.springframework.boot.devtools.restart.RestartApplicationListener.onApplicationStartingEvent(RestartApplicationListener.java:67)
	at org.springframework.boot.devtools.restart.RestartApplicationListener.onApplicationEvent(RestartApplicationListener.java:45)
	at org.springframework.context.event.SimpleApplicationEventMulticaster.doInvokeListener(SimpleApplicationEventMulticaster.java:172)
	at org.springframework.context.event.SimpleApplicationEventMulticaster.invokeListener(SimpleApplicationEventMulticaster.java:165)
	at org.springframework.context.event.SimpleApplicationEventMulticaster.multicastEvent(SimpleApplicationEventMulticaster.java:139)
	at org.springframework.context.event.SimpleApplicationEventMulticaster.multicastEvent(SimpleApplicationEventMulticaster.java:122)
	at org.springframework.boot.context.event.EventPublishingRunListener.starting(EventPublishingRunListener.java:69)
	at org.springframework.boot.SpringApplicationRunListeners.starting(SpringApplicationRunListeners.java:48)
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:292)
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1118)
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1107)
	at com.crypto.MarketPlaceApplication.main(MarketPlaceApplication.java:10)

Please make sure you are using jdk 1.8 (spring boot 1.* issues)


